package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import project.dao.CaregiverRepository;
import project.model.Caregiver;
import project.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Service layers used to validate the information given for the project queries.
 *
 * @author Codrut
 */
public class CaregiverServiceLayer {

    @Autowired
    private CaregiverRepository caregiverRepository;

    /**
     * Return all the available caregivers.
     *
     * @return
     */
    public List<Caregiver> getAllCaregiversList(){
        List<Caregiver> caregivers = new ArrayList<>();
        for (Caregiver caregiver : caregiverRepository.findAll()){
            caregivers.add(caregiver);
        }
        return caregivers;
    }

    /**
     * Return a caregiver by the given id.
     *
     * @param user of the caregiver
     */
    public Caregiver findCaregiveByUser(User user){
        return caregiverRepository.findCaregiverByUser(user);
    }

    /**
     * Create a new caregiver entity in the project.
     *
     * @param caregiver
     */
    public void createCaregiver(Caregiver caregiver){
        caregiverRepository.save(caregiver);
    }

    /**
     * Delete a caregiver entity from the project by the given @id.
     *
     * @param id
     */
    public void deleteCaregiver(Long id){
        caregiverRepository.deleteById(id);
    }

}
