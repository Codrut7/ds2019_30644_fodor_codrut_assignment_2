package project.service;

import org.springframework.beans.factory.annotation.Autowired;
import project.dao.DoctorRepository;
import project.model.Doctor;
import project.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Service layers used to validate the information given for the project queries.
 *
 * @author Codrut
 */
public class DoctorServiceLayer {

    @Autowired
    private DoctorRepository doctorRepository;

    /**
     * Return all the available doctors.
     *
     * @return
     */
    public List<Doctor> getAlDoctorList(){
        List<Doctor> doctors = new ArrayList<>();
        doctorRepository.findAll().forEach(doctors::add);
        return doctors;
    }

    /**
     * Return a doctor by the given id.
     *
     * @param user of the doctor
     */
    public Doctor findDoctorByUser(User user){
        return doctorRepository.findByUser(user);
    }

    /**
     * Create a new doctor entity in the project.
     *
     * @param doctor
     */
    public void createDoctor(Doctor doctor){
        doctorRepository.save(doctor);
    }

    /**
     * Delete a doctor entity from the project by the given @id.
     *
     * @param id
     */
    public void deleteDoctor(Long id){
        doctorRepository.deleteById(id);
    }

}
