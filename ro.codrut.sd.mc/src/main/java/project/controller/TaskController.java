package project.controller;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.apache.tomcat.jni.Local;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.stereotype.Component;
import project.dao.TaskRepository;
import project.model.Task;
import project.utils.TaskHelper;
import project.utils.Validators;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.lang.reflect.Field;
import java.time.LocalDateTime;
import java.util.logging.Logger;

@Component
public class TaskController implements CommandLineRunner {

    private final static String QUEUE_NAME = "PATIENT_QUEUE";
    @Autowired
    private TaskRepository taskRepository;

    @Override
    public void run(String...args) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                Task task  = null;
                ByteArrayInputStream bis = new ByteArrayInputStream(delivery.getBody());
                ObjectInput in = null;
                try {
                    in = new ObjectInputStream(bis);
                    LocalDateTime startDate = (LocalDateTime)in.readObject();
                    LocalDateTime endDate = (LocalDateTime)in.readObject();
                    String activity = (String) in.readObject();
                    task = new Task(startDate,endDate,activity);
                    if(TaskHelper.notifyCaregiver(task)){
                        //notify caregiver
                        System.out.println("---------------------------------------------------------------------");
                    }
                    taskRepository.save(task);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    try {
                        if (in != null) {
                            in.close();
                        }
                    } catch (IOException ex) {
                        // ignore close exception
                    }
                }
            };
            channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }
}