package project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project.model.Patient;
import project.model.User;

/**
 * JPA generic implementation of the repository queries used on patient entity.
 */
@Repository
public interface PatientRepository extends CrudRepository<Patient, Long> {

    Patient findByUser(User user);
}
