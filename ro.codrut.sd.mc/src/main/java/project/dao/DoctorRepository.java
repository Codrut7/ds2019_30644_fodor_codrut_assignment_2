package project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project.model.Doctor;
import project.model.User;

/**
 * JPA generic implementation of the repository queries used on doctor entity.
 */
@Repository
public interface DoctorRepository extends CrudRepository<Doctor, Long> {

    Doctor findByUser(User user);
}
