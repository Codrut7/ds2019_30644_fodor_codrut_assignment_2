package project.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import project.model.Medication;

/**
 * JPA generic implementation of the repository queries used on medication entity.
 */
@Repository
public interface MedicationRepository extends CrudRepository<Medication, Long> {

    Medication findByName(String name);
}
