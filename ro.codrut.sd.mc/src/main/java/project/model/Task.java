package project.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class Task implements Serializable {

    /** ID of the task. */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long taskId;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    private String activity;

    public Task(){

    }

    public Task(LocalDateTime startDate, LocalDateTime endDate, String activity) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.activity = activity;
    }

    public Long getTaskId() {
        return taskId;
    }

    public void setTaskId(Long taskId) {
        this.taskId = taskId;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
