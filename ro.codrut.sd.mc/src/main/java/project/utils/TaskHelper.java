package project.utils;

import project.model.Task;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class TaskHelper {

    public static boolean notifyCaregiver(Task task){
        LocalDateTime tempDateTime = LocalDateTime.from(task.getStartDate());
        if(task.getActivity().equals("Sleeping")){
            tempDateTime = tempDateTime.plusHours(12);
            return tempDateTime.isBefore(task.getEndDate());
        }else if(task.getActivity().equals("Leaving")){
            tempDateTime = tempDateTime.plusHours(12);
            return tempDateTime.isBefore(task.getEndDate());
        }else if(task.getActivity().equals("Toileting")){
            tempDateTime = tempDateTime.plusHours(1);
            return tempDateTime.isBefore(task.getEndDate());
        }
        return false;
    }

}
