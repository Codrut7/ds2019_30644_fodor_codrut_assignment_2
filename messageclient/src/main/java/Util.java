import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Util {

    public static List<Task> processFile(){
        BufferedReader reader;
        List<Task> patientActivities = new ArrayList<>();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        try {
            reader = new BufferedReader(new FileReader(
                    "F:\\facultate\\proiect sd\\messageclient\\activity.txt"));
            String line = reader.readLine();
            while (line != null) {
                StringTokenizer st = new StringTokenizer(line);
                String startTime = st.nextToken() + " " + st.nextToken();
                String endTime = st.nextToken() + " " + st.nextToken();
                String activity = st.nextToken();
                LocalDateTime dateStarTime = LocalDateTime.parse(startTime, formatter);
                LocalDateTime dateEndTime = LocalDateTime.parse(endTime, formatter);
                patientActivities.add(new Task(dateStarTime,dateEndTime,activity));
                // read next line
                line = reader.readLine();
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return patientActivities;
    }

}
