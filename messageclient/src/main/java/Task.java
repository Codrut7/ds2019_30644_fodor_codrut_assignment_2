import java.io.Serializable;
import java.time.LocalDateTime;

public class Task implements Serializable {

    /** Start date of the activity. */
    private LocalDateTime startDate;
    /** End date of the activity. */
    private LocalDateTime endDate;
    /** Name of the activity. */
    private String activity;

    public Task(LocalDateTime startDate, LocalDateTime endDate, String activity) {
        this.startDate = startDate;
        this.endDate = endDate;
        this.activity = activity;
    }

    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    public LocalDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateTime endDate) {
        this.endDate = endDate;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }
}
