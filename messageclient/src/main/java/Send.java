import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import org.springframework.messaging.simp.stomp.StompSession;
import org.springframework.util.concurrent.ListenableFuture;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.List;

import static java.lang.Thread.sleep;

public class Send {

    private final static String QUEUE_NAME = "PATIENT_QUEUE";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        WebsocketClient helloClient = new WebsocketClient();

        ListenableFuture<StompSession> f = helloClient.connect();
        StompSession stompSession = f.get();
        helloClient.subscribeGreetings(stompSession);
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
             channel.queueDeclare(QUEUE_NAME, false, false, false, null);
             for(Task task : Util.processFile()){
                 ByteArrayOutputStream bos = new ByteArrayOutputStream();
                 ObjectOutput out = null;
                 try {
                     out = new ObjectOutputStream(bos);
                     out.writeObject(task.getStartDate());
                     out.writeObject(task.getEndDate());
                     out.writeObject(task.getActivity());
                     out.flush();
                     byte[] bytes = bos.toByteArray();
                     channel.basicPublish("", QUEUE_NAME, null, bytes);
                 } finally {
                     try {
                         bos.close();
                     } catch (IOException ex) {
                         // ignore close exception
                     }
                     sleep(500);
                 }
             }
        }

    }
}
